extern lv_disp_t* initialize_screen();
extern void deinit_screen();

extern int calculate_font_size();

extern void style_battery_icon(lv_obj_t* bat_bar, lv_obj_t* bat_body_top);
extern void position_battery_icon(lv_obj_t* bat_bar, lv_obj_t* bat_body_top);

extern void style_battery_label(lv_obj_t* bat_label, lv_font_t* bat_label_font);
extern void position_battery_label(lv_obj_t* bat_label, lv_obj_t* bat_bar);
