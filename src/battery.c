#include "battery.h"
#include <dirent.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define POWER_SUPPLY_PATH "/sys/class/power_supply"

static int capacity_fd = -1;
static int online_fd = -1;
static int power_supply_fd = -1;
static DIR* power_supply = NULL;

static int find_and_open_file(char* filename) {
	DIR* dir;
	struct dirent* power_supply_entry;
	struct dirent* dir_entry;
	int fd;

	rewinddir(power_supply);
	while((power_supply_entry = readdir(power_supply)) != NULL) {
		dir = fdopendir(openat(power_supply_fd, power_supply_entry->d_name, O_RDONLY));
		if(dir == NULL) {
			return -1;
		}
		while((dir_entry = readdir(dir)) != NULL) {
			if(strcmp(dir_entry->d_name, filename) == 0) {
				fd = openat(dirfd(dir), dir_entry->d_name, O_RDONLY);
				closedir(dir);
				return fd;
			}
		}
		closedir(dir);
	}

	return -1;
}

enum battery_status battery_open() {
	power_supply = opendir(POWER_SUPPLY_PATH);
	if(power_supply == NULL) {
		return BATTERY_ERRNO;
	}
	power_supply_fd = dirfd(power_supply);
	capacity_fd = find_and_open_file("capacity");
	if(capacity_fd == -1) {
		return BATTERY_NOT_FOUND;
	}
	online_fd = find_and_open_file("online");
	if(online_fd == -1) {
		return BATTERY_NOT_FOUND;
	}
	return BATTERY_OK;
}

enum battery_status battery_get(unsigned char* capacity) {
	char online;
	char capacity_data[4];
	int ret;

	ret = read(online_fd, &online, 1);
	if(ret != 1) {
		return BATTERY_ERRNO;
	}
	if(online != '1') {
		return BATTERY_UNPLUGGED;
	}
	lseek(online_fd, 0, SEEK_SET);

	ret = read(capacity_fd, capacity_data, 3);
	if(ret == -1) {
		return BATTERY_ERRNO;
	}
	lseek(capacity_fd, 0, SEEK_SET);
	capacity_data[ret] = '\0';
	*capacity = (unsigned char)strtoul(capacity_data, NULL, 10);
	return BATTERY_OK;
}

enum battery_status battery_close() {
	// TODO: check errno of those calls (if it's even worth it)
	if(capacity_fd != -1)
		close(capacity_fd);
	if(online_fd != -1)
		close(online_fd);
	if(power_supply != NULL)
		closedir(power_supply);
	return BATTERY_OK;
}
