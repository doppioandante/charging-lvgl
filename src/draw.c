#include "lv_drivers/display/fbdev.h"
#include "lvgl/lvgl.h"
#include <stdbool.h>
#include <stdlib.h>

#define COLOR_DARK_GRAY lv_color_make(0x3F, 0x3F, 0x3F)

static uint32_t hor_res;
static uint32_t ver_res;
static bool is_landscape;
static lv_color_t* buf;

lv_disp_t* initialize_screen() {
	fbdev_init();

	fbdev_get_sizes(&hor_res, &ver_res);
	is_landscape = hor_res > ver_res;

	static lv_disp_draw_buf_t disp_buf;
	buf = malloc(sizeof(lv_color_t) * hor_res * ver_res);
	if(buf == NULL) {
		fbdev_exit();
		return NULL;
	}
	lv_disp_draw_buf_init(&disp_buf, buf, NULL, hor_res * ver_res);

	static lv_disp_drv_t disp_drv;
	lv_disp_drv_init(&disp_drv); /*Basic initialization*/
	disp_drv.draw_buf = &disp_buf;
	disp_drv.flush_cb = fbdev_flush;
	disp_drv.hor_res = hor_res;
	disp_drv.ver_res = ver_res;
	return lv_disp_drv_register(&disp_drv);
}

void deinit_screen() {
	fbdev_exit();
	free(buf);
}

int calculate_font_size() {
	int h;
	if(is_landscape)
		h = ver_res / 2.5 * 1.5;
	else
		h = hor_res / 2.5 * 1.5;
	h -= h / 20 * 2;
	h /= 8;
	return h;
}

void style_battery_icon(lv_obj_t* bat_bar, lv_obj_t* bat_body_top) {
	int outline_width;
	static lv_style_t style_bat_bar_bg, style_bat_bar_indic, style_bat_body_top;

	// Calculate outline width of battery bar
	if(is_landscape)
		outline_width = ver_res / 2.5 * 1.5 / 20;
	else
		outline_width = hor_res / 2.5 * 1.5 / 20;

	// Remove old styles
	lv_obj_remove_style_all(bat_bar);
	lv_obj_remove_style_all(bat_body_top);

	// Create style for background of bar
	lv_style_init(&style_bat_bar_bg);
	lv_style_set_bg_color(&style_bat_bar_bg, COLOR_DARK_GRAY);
	lv_style_set_bg_opa(&style_bat_bar_bg, LV_OPA_COVER);
	lv_style_set_outline_color(&style_bat_bar_bg, lv_color_white());
	lv_style_set_outline_width(&style_bat_bar_bg, outline_width);
	lv_style_set_radius(&style_bat_bar_bg, outline_width * 2);

	// Create style for foreground of bar
	lv_style_init(&style_bat_bar_indic);
	lv_style_set_bg_color(&style_bat_bar_indic, lv_palette_main(LV_PALETTE_GREY));
	lv_style_set_bg_opa(&style_bat_bar_indic, LV_OPA_COVER);

	// Create style for small rectangle on top of bar
	lv_style_init(&style_bat_body_top);
	lv_style_set_bg_color(&style_bat_body_top, lv_color_white());
	lv_style_set_bg_opa(&style_bat_body_top, LV_OPA_COVER);
	lv_style_set_radius(&style_bat_body_top, outline_width);

	// Apply created styles
	lv_obj_add_style(bat_bar, &style_bat_bar_bg, LV_PART_MAIN);
	lv_obj_add_style(bat_bar, &style_bat_bar_indic, LV_PART_INDICATOR);
	lv_obj_add_style(bat_body_top, &style_bat_body_top, LV_PART_MAIN);
}

void position_battery_icon(lv_obj_t* bat_bar, lv_obj_t* bat_body_top) {
	int w, h;

	// Rotate bar depending on screen resolution
	if(is_landscape)
		h = ver_res / 2.5,
		w = h * 1.5;
	else
		w = hor_res / 2.5,
		h = w * 1.5;

	lv_obj_set_size(bat_bar, w, h);
	lv_obj_align(bat_bar, LV_ALIGN_CENTER, 0, 0);

	// Rotate top part depending on screen resolution
	if(is_landscape)
		h /= 3,
		w = h / 3;
	else
		w /= 3,
		h = w / 3;

	lv_obj_set_size(bat_body_top, w, h);
	if(is_landscape)
		lv_obj_align_to(bat_body_top, bat_bar, LV_ALIGN_OUT_RIGHT_MID, 0, 0);
	else
		lv_obj_align_to(bat_body_top, bat_bar, LV_ALIGN_OUT_TOP_MID, 0, 0);
}

void style_battery_label(lv_obj_t* bat_label, lv_font_t* bat_label_font) {
	// Remove old style
	lv_obj_remove_style_all(bat_label);

	// Create style for label
	static lv_style_t style_bat_label;
	lv_style_init(&style_bat_label);
	lv_style_set_text_font(&style_bat_label, bat_label_font);
	lv_style_set_text_color(&style_bat_label, lv_color_white());

	// Apply created style
	lv_obj_add_style(bat_label, &style_bat_label, LV_PART_MAIN);
}

void position_battery_label(lv_obj_t* bat_label, lv_obj_t* bat_bar) {
	lv_obj_align(bat_label, LV_ALIGN_CENTER, 0, 0);
}
