// LVGL includes
#include "lv_drivers/display/fbdev.h"
#include "lv_lib_freetype/lv_freetype.h"
#include "lvgl/lvgl.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "battery.h"
#include "draw.h"

static const char status_string_tbl[][47] = {
	[BATTERY_UNKNOWN] = "unknown reason",
	[BATTERY_NOT_FOUND] = "battery not found",
	[BATTERY_ERRNO] = "error while reading battery data (check errno)",
	[BATTERY_UNPLUGGED] = "cable got unplugged",
	[BATTERY_OK] = "battery is charging"
};

void set_battery_level(lv_obj_t* bat_bar, lv_obj_t* bat_label, int bat_level) {
	lv_label_set_text_fmt(bat_label, "%i%%\0", bat_level);
	lv_obj_align(bat_label, LV_ALIGN_CENTER, 0, 0);
	lv_bar_set_value(bat_bar, bat_level, LV_ANIM_OFF);
}

void exit_gracefully(enum battery_status reason) {
	deinit_screen();
	battery_close();
	fprintf(stderr, "Exiting charging-lvgl, reason: %s.\n", status_string_tbl[reason]);
	exit(0);
}

int main(int argc, char** argv) {
	unsigned char bat_percentage = 0;
	enum battery_status status = BATTERY_UNKNOWN;

	if(argc != 2) {
		printf("Path to font isn't set!\n");
		return 1;
	}

	// Initialize LVGL
	lv_init();

	// Initialize screen
	lv_disp_t* disp = initialize_screen();
	if(disp == NULL) {
		fprintf(stderr, "Exiting charging-lvgl, reason: "
						"NULL returned while allocating memory for framebuffer");
		fbdev_exit();
		exit(1);
	}

	// Initialize battery
	status = battery_open();
	if(status != BATTERY_OK) {
		exit_gracefully(status);
	}

	// Change background color of screen to black
	lv_disp_set_bg_color(disp, lv_color_black());

	// Load font for label
	static lv_ft_info_t font_bat_label;
	bool lv_ft_err;
	lv_ft_err = lv_freetype_init(11, 1, 0); // 10 numbers + "%" symbol, 1 size
	assert(lv_ft_err == true);
	font_bat_label.name = argv[1];
	font_bat_label.weight = calculate_font_size();
	font_bat_label.style = FT_FONT_STYLE_NORMAL;
	lv_ft_err = lv_ft_font_init(&font_bat_label);
	assert(lv_ft_err == true);

	// Create a bar
	lv_obj_t* bat_bar = lv_bar_create(lv_scr_act());
	lv_bar_set_range(bat_bar, 0, 100);
	// Create small rectangle on top of bar
	lv_obj_t* bat_body_top = lv_obj_create(lv_scr_act());
	// Create label containing current battery charge
	lv_obj_t* bat_label = lv_label_create(bat_bar);

	// Style created battery icon
	style_battery_icon(bat_bar, bat_body_top);
	// Display created battery icon on screen
	position_battery_icon(bat_bar, bat_body_top);

	// Style created label
	style_battery_label(bat_label, font_bat_label.font);
	// Display created label on screen
	position_battery_label(bat_label, bat_bar);

	while(1) {
		status = battery_get(&bat_percentage);

		if(status == BATTERY_OK) {
			set_battery_level(bat_bar, bat_label, bat_percentage);
		} else {
			exit_gracefully(status);
		}
		// Periodically call the lv_task_handler()
		lv_task_handler();
		usleep(5000);
	}

	return 0;
}
